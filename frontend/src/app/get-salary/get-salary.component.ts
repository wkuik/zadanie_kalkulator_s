import { Component, OnInit } from "@angular/core";
import { DailySalaryResponse } from "../model/DailySalaryResponse";
import { BackendService } from "../services/backend.service";

@Component({
  selector: "get-salary",
  templateUrl: "./get-salary.component.html",
  styleUrls: ["./get-salary.component.scss"],
  providers: [ BackendService ]
})

export class GetSalaryComponent implements OnInit {
  salary: number;
  dailySalaryResponse: DailySalaryResponse;
  showProgress = false;
  countries: Map<String, String>;
  country: String;

  constructor(private backendService: BackendService){}

  ngOnInit() {
    this.getCountries()
  }

  getCountries() {
    this.backendService.getCountries().subscribe(result => {
      this.countries = result;

      this.countries = result;
    });
  }

   getSalary(country, netSalary) {
     this.backendService.getNetSalary(country, netSalary).subscribe(result => {
       this.dailySalaryResponse = result;
       this.showProgress = false;
     });
   }

   getCountriesKeys() {
    return Object.keys(this.countries);
   }

   getSelectedCurrency() {
    return (this.countries !== undefined && this.country !== undefined)
      ? Object.keys(this.countries).filter(key => key == this.country).map(key => this.countries[key]).shift()
      : ''
   }
}
