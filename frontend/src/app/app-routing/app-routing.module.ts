import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GetSalaryComponent } from '../get-salary/get-salary.component'

const routes: Routes = [
  {
    path: '',
    component: GetSalaryComponent,
  },
];

@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule { }
