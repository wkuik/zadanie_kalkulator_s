import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DailySalaryResponse } from "../model/DailySalaryResponse";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  uri = environment.uri;
  countriesUri = environment.countriesUri;

  constructor(private http: HttpClient) { }

  getCountries() {
    return this.http.get<Map<String, String>>(this.countriesUri);
  }

  getNetSalary(country, dailySalary) {
    return this.http.get<DailySalaryResponse>(this.uri + '/' + country + '/' + dailySalary);
  }
}

