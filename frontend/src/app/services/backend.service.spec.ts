import { TestBed } from '@angular/core/testing';
import { BackendService } from './backend.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpTestingController } from "../../../node_modules/@angular/common/http/testing";

describe('BackendService', () => {
  let backendService: BackendService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [BackendService]
  }).compileComponents()
  );

  beforeEach(() => {
    backendService = TestBed.get(BackendService);
    httpMock = TestBed.get(HttpTestingController);
  });

   it('should be created', () => {
     const service: BackendService = TestBed.get(BackendService);
     expect(service).toBeTruthy();
   });

  it('should successfully get query and top', (done) => {
    backendService.getNetSalary("PL", "1000.5")
      .subscribe(res => {
        expect(res).toEqual(
            "16856.91"
        );
        done();
      });

    let request1 = httpMock.expectOne('/calc/PL/1000.5');
    request1.flush("16856.91");

    httpMock.verify();
  });

});
