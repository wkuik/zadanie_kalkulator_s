package com.calculator.backend.exception;

public class UnsupportedCountryException extends RuntimeException {
    public UnsupportedCountryException(String description) {
        super(description);
    }
}
