package com.calculator.backend.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.io.IOException;
import java.math.BigDecimal;

@Named
public class ResponseParser {

    Logger logger = LoggerFactory.getLogger(getClass());
    private final String LOG_HEADER = this.getClass().getSimpleName();

    private ObjectMapper mapper;

    @PostConstruct
    void init() {
        mapper = new ObjectMapper();
    }

    public BigDecimal parse(String response) {
        BigDecimal exchangeRate = null;

        try {
            ObjectNode node = mapper.readValue(response, ObjectNode.class);
            JsonNode jsonNode = node.findPath("mid");
            if (jsonNode.isValueNode())
                exchangeRate = jsonNode.decimalValue();
            else
                logger.warn("{}\n{}: {}",
                        LOG_HEADER,
                        "ResponseParser warning",
                        "lack of data or corrupted");

        } catch (IOException e) {
            logger.warn("{}\n{}: {}",
                    LOG_HEADER,
                    "ResponseParser exception",
                    e.getStackTrace());
        }

        return exchangeRate;
    }
}
