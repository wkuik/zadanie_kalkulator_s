package com.calculator.backend.model;

import com.calculator.backend.providers.ExchangeRateGetter;

import java.math.BigDecimal;

public class PaymentData {
    private BigDecimal tax;
    private BigDecimal fixedExpenses;
    private ExchangeRateGetter exchangeRateGetter;
    private CurrencyCode currencyCode;

    public CurrencyCode getCurrencyCode() { return currencyCode; }

    public BigDecimal getTax() {
        return tax;
    }

    public BigDecimal getFixedExpenses() {
        return fixedExpenses;
    }

    public BigDecimal getExchangeRateToPLN() {
        return exchangeRateGetter.getExchangeRate(currencyCode);
    }

    public PaymentData withTax(BigDecimal tax) {
        this.tax = tax;
        return this;
    }

    public PaymentData withFixedExpenses(BigDecimal fixedExpenses) {
        this.fixedExpenses = fixedExpenses;
        return this;
    }

    public PaymentData registerExchangeRateGetter(ExchangeRateGetter exchangeRateGetter, CurrencyCode currencyCode) {
        this.exchangeRateGetter = exchangeRateGetter;
        this.currencyCode = currencyCode;
        return this;
    }
}
