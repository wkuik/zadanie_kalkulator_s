package com.calculator.backend.model;

import com.calculator.backend.exception.UnsupportedCountryException;
import com.calculator.backend.providers.ExchangeRateProvider;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;

@Service
public class PaymentDataFactory {

    @Inject
    private ExchangeRateProvider exchangeRateProvider;

    public PaymentDataFactory(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    public PaymentData getPaymentData(CountryCode countryCode) {
        switch (countryCode) {
            case PL:
                return forPL();
            case UK:
                return forUK();
            case DE:
                return forDE();
            default:
                throw new UnsupportedCountryException("Country " + countryCode + " not supported!");
        }
    }

    private PaymentData forPL() {
        return new PaymentData()
                .registerExchangeRateGetter(currencyCode -> BigDecimal.ONE, CurrencyCode.PLN)
                .withFixedExpenses(BigDecimal.valueOf(1200))
                .withTax(BigDecimal.valueOf(0.19));
    }

    private PaymentData forUK() {
        return new PaymentData()
                .registerExchangeRateGetter(exchangeRateProvider, CurrencyCode.GBP)
                .withFixedExpenses(BigDecimal.valueOf(600))
                .withTax(BigDecimal.valueOf(0.25));
    }

    private PaymentData forDE() {
        return new PaymentData()
                .registerExchangeRateGetter(exchangeRateProvider, CurrencyCode.EUR)
                .withFixedExpenses(BigDecimal.valueOf(800))
                .withTax(BigDecimal.valueOf(0.20));
    }
}
