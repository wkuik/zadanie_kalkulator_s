package com.calculator.backend.model;

public enum CurrencyCode {
    PLN, GBP, EUR
}
