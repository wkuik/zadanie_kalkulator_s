package com.calculator.backend.model;

public enum CountryCode {
    PL, UK, DE
}
