package com.calculator.backend.controller;

import com.calculator.backend.model.CountryCode;
import com.calculator.backend.model.CurrencyCode;
import com.calculator.backend.providers.NetSalaryProvider;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class NetSalaryController {

    @Inject
    private NetSalaryProvider netSalaryProvider;

    @ApiOperation(
            "Returns the monthly net wage at the given country and daily rate")
    @GetMapping("/calc/{country}/{dailyRate}")
    public BigDecimal getNetSalary(
            @PathVariable("country") CountryCode country,
            @PathVariable("dailyRate") BigDecimal dailyRate) {
        return netSalaryProvider.getNetSalary(country, dailyRate);
    }

    @ApiOperation(
            "Returns the available countries with assigned currencies")
    @GetMapping("/countries")
    public Map<CountryCode, CurrencyCode> getCountriesAndCurrencies() {
        return netSalaryProvider.getCountriesAndCurrencies();
    }
}
