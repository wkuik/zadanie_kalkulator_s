package com.calculator.backend.providers;

import com.calculator.backend.model.CountryCode;
import com.calculator.backend.model.CurrencyCode;
import com.calculator.backend.model.PaymentData;
import com.calculator.backend.model.PaymentDataFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

@Service
public class NetSalaryProvider {

    @Value("${paid.days}")
    private Integer PAID_DAYS;

    @Inject
    private PaymentDataFactory paymentDataFactory;

    public NetSalaryProvider(PaymentDataFactory paymentDataFactory) {
        this.paymentDataFactory = paymentDataFactory;
    }

    public BigDecimal getNetSalary(CountryCode country, BigDecimal dailyRate) {
        PaymentData paymentData = paymentDataFactory.getPaymentData(country);
        BigDecimal revenue = dailyRate.multiply(BigDecimal.valueOf(PAID_DAYS));
        BigDecimal income = revenue.subtract(paymentData.getFixedExpenses());
        BigDecimal netSalary = income.multiply(BigDecimal.ONE.subtract(paymentData.getTax()));
        BigDecimal ret = netSalary.multiply(paymentData.getExchangeRateToPLN());
        return ret.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public Map<CountryCode, CurrencyCode> getCountriesAndCurrencies() {
        Map map = new HashMap();
        Arrays.stream(CountryCode.values())
                .forEach(countryCode -> map.put(countryCode, paymentDataFactory.getPaymentData(countryCode).getCurrencyCode()));
        return map;
    }
}
