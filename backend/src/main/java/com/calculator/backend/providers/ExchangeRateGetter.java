package com.calculator.backend.providers;

import com.calculator.backend.model.CurrencyCode;

import java.math.BigDecimal;

public interface ExchangeRateGetter {
    BigDecimal getExchangeRate(CurrencyCode currencyCode);
}
