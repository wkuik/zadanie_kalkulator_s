package com.calculator.backend.providers;

import com.calculator.backend.model.ResponseParser;
import com.calculator.backend.model.CurrencyCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.math.BigDecimal;

@Service
public class ExchangeRateProvider implements ExchangeRateGetter {

    Logger logger = LoggerFactory.getLogger(getClass());
    private final String LOG_HEADER = this.getClass().getSimpleName();

    @Value("${base.api.uri}")
    private String BASE_URL;

    @Value("${endpoint.uri}")
    private String ENDPOINT;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Inject
    private ResponseParser responseParser;

    @Override
    public BigDecimal getExchangeRate(CurrencyCode currencyCode) {
        String url = String.format("%s%s%s", BASE_URL, ENDPOINT, currencyCode);

        logger.info(String.format("url: %s", url));
        BigDecimal exchangeRate = responseParser.parse(getFromBackend(url));
        return exchangeRate;
    }

    private String getFromBackend(String url) {
        ResponseEntity<String> response =
                restTemplate().exchange(url, HttpMethod.GET, buildHttpEntity(), String.class);

        logger.info(String.format("response: %s", response.getBody()));
        String responseBody = null;

        if (response.getStatusCode() == HttpStatus.OK) {
            responseBody = response.getBody();
            logger.info("{} {} {}: {}",
                    LOG_HEADER,
                    response.getStatusCode(),
                    response.getStatusCode().getReasonPhrase(),
                    url);
            logger.debug("Response: {}",
                    responseBody);
        } else {
            logger.warn("{}\n{} {}: {}",
                    LOG_HEADER,
                    response.getStatusCode(),
                    response.getStatusCode().getReasonPhrase(),
                    url);
        }

        return responseBody;
    }

    private HttpEntity<?> buildHttpEntity() {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return new HttpEntity<>(headers);
    }
}
