package com.calculator.backend.controller;

import com.calculator.backend.model.CountryCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class NetSalaryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetNetSalary() throws Exception {
        BigDecimal dailyRate = BigDecimal.valueOf(1000.5);
        CountryCode countryCode = CountryCode.PL;
        mockMvc.perform(get("/calc/{countryCode}/{dailyRate}", countryCode, dailyRate))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().bytes(new String("16856.91").getBytes()));
    }
}
