package com.calculator.backend.providers;

import com.calculator.backend.model.CountryCode;
import com.calculator.backend.model.CurrencyCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NetSalaryProviderTest {

    @Autowired
    NetSalaryProvider netSalaryProvider;

    @Test
    public void testGetSupportedCountries() {
        Map<CountryCode, CurrencyCode> map = netSalaryProvider.getCountriesAndCurrencies();

        assertTrue(map.size() == CountryCode.values().length);
    }
}
