# Salary calculator application

The application consists of two parts - frontend created in Angular CLI and backend - created using the Spring Boot. The application allows you to calculate the payment on a contract in different countries, including fixed costs and tax. The backend part communicates with the nbp.pl API in order to get the exchange rates.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This project was created under Windows 7 Ultimate 64-bit, but there should be no problem running and building it on other systems. I used the IntelliJ IDEA 2018.2.1 IDE to create the project.

* [Git 2.19](https://git-scm.com/) - Version Control System
* [JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java Development Kit
* [Maven 3.5.4](https://maven.apache.org/) - Dependency Management

Make sure JDK is installed, and “JAVA_HOME” variable is added as Windows environment variable.
This project was created with jdk1.8.0_181. Download and install Apache Maven, Git and Docker.

### Clone
To get started you can simply clone this repository using git:
```
git clone git@bitbucket.org:wkuik/zadanie_kalkulator_s.git
cd zadanie_kalkulator_s
```

### Installing

In order to build a project, you must call the command:
```
mvnw.cmd clean package
```

As a result, we will receive ready packages with a server and web part.
Running the backend:

```
java -jar backend\target\backend-0.0.1-SNAPSHOT.jar
```

The backend part will be available at localhost's port 8080.

Running the frontend:
```
cd frontend
npm start
```

The frontend part will be available at localhost's port 4200.

## Documentation

Documentation is provided with Swagger:
```
http://localhost:8080/swagger-ui.html
```

## Running the tests

In order to run the tests of the web application, from frontend folder run:
```
ng test
```

## Author

* **Wojciech Kuik**
